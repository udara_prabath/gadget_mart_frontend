import React, {Component} from 'react';
import Header from '../containers/default-layout/Header';
import Footer from '../containers/default-layout/Footer';
import Headers from "../containers/default-layout/Header";

export default class ContactUs extends Component {
  render() {
    return (
      <div>
        <Header  history={this.props.history}/>
        <main className="main">
          <nav aria-label="breadcrumb" className="breadcrumb-nav">
            <div className="container">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="/">
                    <i className="icon-home" />
                  </a>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  Contact Us
                </li>
              </ol>
            </div>
          </nav>

          <div className="container mb-8">
            <div id="maps">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15843.26861710818!2d79.87019093164365!3d6.912453421879484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae259a291b991db%3A0xe37fc789128a67fc!2sColombo%2008%2C%20Colombo!5e0!3m2!1sen!2slk!4v1596292601867!5m2!1sen!2slk"
                frameborder="0"
                style="border:0;"
                allowfullscreen=""
                aria-hidden="false"
                tabindex="0"
                style={{height: 450, width: 100 + '%'}}
              ></iframe>
            </div>

            <div className="row">
              <div className="col-md-8">
                <h2 className="light-title">
                  Write <strong>Us</strong>
                </h2>

                <form action="#">
                  <div className="form-group required-field">
                    <label htmlFor="contact-name">Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="contact-name"
                      name="contact-name"
                      required
                    />
                  </div>

                  <div className="form-group required-field">
                    <label htmlFor="contact-email">Email</label>
                    <input
                      type="email"
                      className="form-control"
                      id="contact-email"
                      name="contact-email"
                      required
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="contact-phone">Phone Number</label>
                    <input
                      type="tel"
                      className="form-control"
                      id="contact-phone"
                      name="contact-phone"
                    />
                  </div>

                  <div className="form-group required-field">
                    <label htmlFor="contact-message">
                      What’s on your mind?
                    </label>
                    <textarea
                      cols="30"
                      rows="1"
                      id="contact-message"
                      className="form-control"
                      name="contact-message"
                      required
                    />
                  </div>

                  <div className="form-footer">
                    <button type="submit" className="btn btn-primary">
                      Submit
                    </button>
                  </div>
                </form>
              </div>

              <div className="col-md-4">
                <h2 className="light-title">
                  Contact <strong>Details</strong>
                </h2>

                <div className="contact-info">
                  <div>
                    <i className="icon-phone" />
                    <p>
                      <a href="tel:">(011) 203 2032</a>
                    </p>
                  </div>
                  <div>
                    <i className="icon-mail-alt" />
                    <p>
                      <a href="mailto:#">gadgetmart@gmail.com</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}
