import React, {Component} from 'react';
import Footer from '../containers/default-layout/Footer';
import Header from '../containers/default-layout/Header';
import Headers from "../containers/default-layout/Header";

export default class AboutUs extends Component {
  render () {
    return (
      <div>
        <Header history={this.props.history} />
        <main className="main">
          <div className="container mt-2 pt-0">
            <div className="outer-container page-header page-header-bg text-left about-us-header-img">
              {/* src={require ('../assets/images/page-header-bg.jpg')}
                style="{{background : '70%/cover #D4E1EA url('assets/images/page-header-bg.jpg')'}}" */}
              <div className="container pl-5">
                <h1>
                  <span>ABOUT US</span>
                  OUR COMPANY
                </h1>
                <h5 onClick={()=>this.props.history.push('/contactUs')} className="btn btn-dark">
                  Contact
                </h5>
              </div>
            </div>
          </div>

          <nav aria-label="breadcrumb" className="breadcrumb-nav">
            <div className="container">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="index.html">
                    <i className="icon-home" />
                  </a>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  About Us
                </li>
              </ol>
            </div>
          </nav>

          <div className="container pt-0">
            <div className="about-section">
              <h2 className="subtitle">About Us</h2>
              <p>
                Located in Colombo, Sri Lanka, Gadget Mart is one of the leading retail company.
                For over two decades we have been producing high
                quality productsour customers.
                Due to our strong focus on best up-to-date quality and constantly
                improving cutting-edge technology, that often exceeds customer expectations,
                we now deliver products to a wide variety of respectable brands and clients
                throughout the world.
              </p>

            </div>
          </div>

          <div className="container pt-0">
            <div className="features-section">
              <h2 className="subtitle">WHY CHOOSE US</h2>

              <div className="row">
                <div className="col-lg-4">
                  <div className="feature-box bg-white">
                    <i className="icon-shipped" />

                    <div className="feature-box-content">
                      <h3>Free Shipping</h3>
                      <p>
                        Free Shipping world wide with safe{' '}
                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="feature-box bg-white">
                    <i className="icon-us-dollar" />

                    <div className="feature-box-content">
                      <h3>100% Money Back Guarantee</h3>
                      <p>
                        100% money back guarantee and free returns
                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="feature-box bg-white">
                    <i className="icon-online-support" />

                    <div className="feature-box-content">
                      <h3>Online Support 24/7</h3>
                      <p>
                        We are 24/7 online for your inquires
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </main>
        <Footer />
      </div>
    );
  }
}
